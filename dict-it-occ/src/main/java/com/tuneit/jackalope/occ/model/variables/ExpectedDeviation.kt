package com.tuneit.jackalope.occ.model.variables

enum class ExpectedDeviation : IntensityVariable {
    HIGH,
    LOW
}