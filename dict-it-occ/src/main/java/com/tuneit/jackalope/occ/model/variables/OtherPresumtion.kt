package com.tuneit.jackalope.occ.model.variables

enum class OtherPresumtion : EventBasedVariable {
    DESIRABLE,
    UNDESIRABLE
}