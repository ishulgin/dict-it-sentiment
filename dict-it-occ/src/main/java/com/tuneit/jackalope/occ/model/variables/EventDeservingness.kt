package com.tuneit.jackalope.occ.model.variables

enum class EventDeservingness : IntensityVariable {
    HIGH,
    LOW
}