package com.tuneit.jackalope.occ.model.variables

enum class Prospect : EventBasedVariable {
    POSITIVE,
    NEGATIVE
}