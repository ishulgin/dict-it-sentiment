package com.tuneit.jackalope.occ.model.variables

enum class DirectionOfEmotion : AgentBasedVariable {
    SELF,
    OTHER
}