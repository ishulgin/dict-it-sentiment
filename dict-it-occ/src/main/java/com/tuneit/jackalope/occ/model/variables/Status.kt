package com.tuneit.jackalope.occ.model.variables

enum class Status : EventBasedVariable {
    UNCONFIRMED,
    CONFIRMED,
    DISCONFIRMED
}