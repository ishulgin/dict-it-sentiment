package com.tuneit.jackalope.occ.model.variables

enum class EventFamiliarity : IntensityVariable {
    COMMON,
    UNCOMMON
}