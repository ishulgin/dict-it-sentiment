package com.tuneit.jackalope.occ.model.variables

enum class EffortOfAction : IntensityVariable {
    OBVIOUS,
    NOT_OBVIOUS
}