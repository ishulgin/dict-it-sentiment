package com.tuneit.jackalope.occ.model.variables

enum class SelfPresumption : EventBasedVariable {
    DESIRABLE,
    UNDESIRABLE
}