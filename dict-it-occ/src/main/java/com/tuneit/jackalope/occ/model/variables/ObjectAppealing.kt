package com.tuneit.jackalope.occ.model.variables

enum class ObjectAppealing : ObjectBasedVariable {
    ATTRACTIVE,
    NOT_ATTRACTIVE
}