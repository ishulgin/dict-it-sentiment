package com.tuneit.jackalope.occ.model.variables

enum class Unexpectedness : EventBasedVariable {
    TRUE,
    FALSE
}