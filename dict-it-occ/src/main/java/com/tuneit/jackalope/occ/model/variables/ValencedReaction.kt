package com.tuneit.jackalope.occ.model.variables

enum class ValencedReaction : EventBasedVariable {
    TRUE,
    FALSE
}