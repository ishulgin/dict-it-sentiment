package com.tuneit.jackalope.occ.model.variables

enum class AgentFondness : AgentBasedVariable {
    LIKED,
    NOT_LIKED
}