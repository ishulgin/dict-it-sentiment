package com.tuneit.jackalope.occ.model.variables

enum class ObjectFondness : ObjectBasedVariable {
    LIKED,
    NOT_LIKED
}