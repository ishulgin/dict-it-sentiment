package com.tuneit.jackalope.occ.model.variables

enum class SelfAppraisal : EventBasedVariable {
    PRAISEWORTHY,
    BLAMEWORTHY
}