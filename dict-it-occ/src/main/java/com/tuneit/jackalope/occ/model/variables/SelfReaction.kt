package com.tuneit.jackalope.occ.model.variables

enum class SelfReaction : EventBasedVariable {
    PLEASED,
    DISPLEASED
}