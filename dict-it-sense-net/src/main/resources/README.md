| Ресурсы | Ссылка для загрузки |                                    
| --- | --- |                                     
| dict.tsv <br/> nndep.rus.model90.9_88.6.txt.gz <br/> russian-ud-mf.tagger <br/> russian-ud-pos.tagger | [Скачать](https://drive.google.com/file/d/1EVMffW4S0A7YIM35eJ74wlTZDU_hT-Gq/) |
| wiktSnap | [Скачать](https://drive.google.com/open?id=12eO3RFy1xAI2gOadmDLFmgeQz8w6-X6M) |
| tonalities.txt | [Скачать](https://drive.google.com/open?id=15ZyqJVX-MMs9JAC670ZKuTalbD5m2Y_N) |