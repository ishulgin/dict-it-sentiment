package com.tuneit.jackalope

import com.tuneit.jackalope.config.Config
import com.tuneit.jackalope.sentiment.TextProcessor
import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication

@SpringBootApplication
open class App

fun main(args: Array<String>) {
    val context = SpringApplication.run(Config::class.java, *args)
    val bean = context.getBean(TextProcessor::class.java)
    bean.processText("Я не видел Джона несколько часов; Я думал, что он может пропустить рейс, но внезапно я увидел его в самолете.")
}