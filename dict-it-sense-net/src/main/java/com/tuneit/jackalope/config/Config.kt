package com.tuneit.jackalope.config

import com.tuneit.jackalope.dict.wiki.engine.api.EngineContext
import com.tuneit.jackalope.dict.wiki.engine.api.WikiEngine
import com.tuneit.jackalope.dict.wiki.engine.api.WikiEngineImpl
import com.tuneit.jackalope.dict.wiki.engine.core.ru.RuEngineContext
import com.tuneit.jackalope.dict.wiki.engine.utils.WiktionarySnapshot
import com.tuneit.jackalope.dict.wiki.engine.utils.WiktionarySnapshotManager
import com.tuneit.jackalope.sentiment.NlpLibraryFacade
import com.tuneit.jackalope.sentiment.stanford.StanfordNlpFacade
import com.tuneit.jackalope.sentiment.SnapshotFacade
import com.tuneit.jackalope.sentiment.TextProcessor
import com.tuneit.jackalope.sentiment.stanford.WordUtils
import edu.stanford.nlp.international.russian.process.RussianProcessor
import org.springframework.beans.factory.annotation.Value
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration

@Configuration
open class Config {

    @Value("\${nlp.parserModelPath}")
    private val parserModelPath: String? = null

    @Value("\${nlp.taggerPath}")
    private val taggerPath: String? = null

    @Value("\${nlp.mfPath}")
    private val mfPath: String? = null

    @Value("\${nlp.dictPath}")
    private val dictPath: String? = null

    @Value("\${nlp.tonalitiesPath}")
    private val tonalitiesPath: String? = null

    @Value("\${snapshot.path}")
    private val snapshotPath: String? = null

    @Bean
    open fun wordUtils(): WordUtils {
        return WordUtils()
    }

    @Bean
    open fun engineContext(): EngineContext {
        return RuEngineContext()
    }

    @Bean
    open fun wikiEngine(engineContext: EngineContext): WikiEngine {
        return WikiEngineImpl(engineContext)
    }

    @Bean
    open fun wiktionarySnapshotManager(wikiEngine: WikiEngine): WiktionarySnapshotManager {
        return WiktionarySnapshotManager(wikiEngine)
    }

    @Bean
    open fun russianProcessor(): RussianProcessor {
        return RussianProcessor(getResourcePath(parserModelPath),
                getResourcePath(taggerPath),
                getResourcePath(mfPath),
                getResourcePath(dictPath),
                getResourcePath(tonalitiesPath))
    }

    @Bean
    open fun nlpLibraryFacade(russianProcessor: RussianProcessor, wordUtils: WordUtils): NlpLibraryFacade {
        return StanfordNlpFacade(russianProcessor, wordUtils)
    }

    @Bean
    open fun wiktionarySnapshot(wiktionarySnapshotManager: WiktionarySnapshotManager): WiktionarySnapshot {
        return wiktionarySnapshotManager.openSnapshot(getResourcePath(snapshotPath))
    }

    @Bean
    open fun snapshotFacade(wiktionarySnapshot: WiktionarySnapshot): SnapshotFacade {
        return SnapshotFacade(wiktionarySnapshot)
    }

    @Bean
    open fun textProcessor(nlpLibraryFacade: NlpLibraryFacade): TextProcessor {
        return TextProcessor(nlpLibraryFacade)
    }

    private fun getResourcePath(resource: String?): String {
        return this.javaClass.classLoader.getResource(resource)!!.path
    }
}
