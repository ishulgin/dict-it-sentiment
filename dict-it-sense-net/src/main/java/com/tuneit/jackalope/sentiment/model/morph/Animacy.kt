package com.tuneit.jackalope.sentiment.model.morph

import com.tuneit.jackalope.sentiment.model.morph.MorphCharacteristic

enum class Animacy : MorphCharacteristic {
    ANIM,
    INANIM
}
