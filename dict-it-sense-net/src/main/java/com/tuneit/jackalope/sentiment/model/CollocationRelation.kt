package com.tuneit.jackalope.sentiment.model

class CollocationRelation(val collocation: Collocation,
                          val type: CollocationRelationType)
