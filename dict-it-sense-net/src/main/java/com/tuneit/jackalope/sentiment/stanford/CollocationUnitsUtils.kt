package com.tuneit.jackalope.sentiment.stanford

import com.tuneit.jackalope.sentiment.model.*
import edu.stanford.nlp.international.russian.process.SensesCountAnnotation
import edu.stanford.nlp.ling.IndexedWord
import edu.stanford.nlp.semgraph.SemanticGraph

//TODO set valence, senses number
class CollocationUnitsUtils(private val wordUtils: WordUtils, val graph: SemanticGraph) {
    fun convertAction(word: IndexedWord): Action {
        val convertedWord = convertWord(word)
        return Action(convertedWord, getValence(convertedWord.value), findRelations(word), getSensesCount(word))
    }

    fun convertSubject(word: IndexedWord): Subject {
        val convertedWord = convertWord(word)
        return Subject(convertedWord, getValence(convertedWord.value), findRelations(word), getSensesCount(word))
    }

    fun convertObject(word: IndexedWord): Object {
        val convertedWord = convertWord(word)
        return Object(convertedWord, getValence(convertedWord.value), findRelations(word), getSensesCount(word))
    }

    private fun findRelations(word: IndexedWord): List<CollocationUnit> {
        val adverbials = ArrayList<Adverbial>()
        val attributes = ArrayList<Attribute>()
        graph.getChildren(word).forEach {
            when (wordUtils.getPartOfSpeech(it)) {
                PartOfSpeech.ADVERB -> adverbials.add(convertAdverbial(it))
                PartOfSpeech.ADJECTIVE -> attributes.add(convertAttribute(it))
            }
        }
        return adverbials + attributes
    }

    private fun convertAdverbial(word: IndexedWord): Adverbial {
        val convertedWord = convertWord(word)
        return Adverbial(convertedWord, getValence(convertedWord.value), findRelations(word), getSensesCount(word))
    }

    private fun convertAttribute(word: IndexedWord): Attribute {
        val convertedWord = convertWord(word)
        return Attribute(convertedWord, getValence(convertedWord.value), findRelations(word), getSensesCount(word))
    }

    private fun getSensesCount(word: IndexedWord): SensesCount {
        val tonalitiesCount = word.get(SensesCountAnnotation::class.java)
        return SensesCount(tonalitiesCount.first, tonalitiesCount.second, tonalitiesCount.third)
    }

    private fun getValence(lemma: String): Double {
        return 0.0
    }

    private fun convertWord(word: IndexedWord): Word {
        return wordUtils.processWord(word, isNegation(word))
    }

    private fun isNegation(word: IndexedWord): Boolean {
        return graph.getChildren(word).any { it.value() == "не" }
    }
}