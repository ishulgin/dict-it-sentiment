package com.tuneit.jackalope.sentiment.model

import com.tuneit.jackalope.sentiment.model.morph.MorphCharacteristic

class Word(val value: String, val negation: Boolean, val partOfSpeech: PartOfSpeech, val morphCharacteristics: Map<String, MorphCharacteristic>) {
    override fun toString(): String {
        return "${if (negation) "не " else ""}$value($partOfSpeech): $morphCharacteristics"
    }
}
