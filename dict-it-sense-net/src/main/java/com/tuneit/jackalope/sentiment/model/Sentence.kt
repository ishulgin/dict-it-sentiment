package com.tuneit.jackalope.sentiment.model

class Sentence(val collocation: Collocation?, valence: Double?) : Sentimental(valence)
