package com.tuneit.jackalope.sentiment

import com.tuneit.jackalope.sentiment.model.Collocation

interface NlpLibraryFacade {
    fun extractTextCollocations(text: String): List<List<Collocation>>
}