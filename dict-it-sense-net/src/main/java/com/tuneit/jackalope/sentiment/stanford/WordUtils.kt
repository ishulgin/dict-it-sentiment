package com.tuneit.jackalope.sentiment.stanford

import com.tuneit.jackalope.sentiment.model.PartOfSpeech
import com.tuneit.jackalope.sentiment.model.Word
import com.tuneit.jackalope.sentiment.model.morph.*
import com.tuneit.jackalope.sentiment.model.morph.Number
import edu.stanford.nlp.ling.CoreAnnotations
import edu.stanford.nlp.ling.IndexedWord

class WordUtils {
    private val morphConverter = StanfordNlpMorphConverter()
    private val partOfSpeechConverter = StanfordNlpPartOfSpeechConverter()

    fun processWord(word: IndexedWord, negation: Boolean): Word {
        return Word(lemmatize(word), negation, getPartOfSpeech(word), getMorph(word))
    }

    fun lemmatize(word: IndexedWord): String {
        return word.get(CoreAnnotations.LemmaAnnotation::class.java) ?: word.value()
    }

    fun getPartOfSpeech(word: IndexedWord): PartOfSpeech {
        return partOfSpeechConverter.convert(extractPartOfSpeech(word))
    }

    fun getMorph(word: IndexedWord): Map<String, MorphCharacteristic> {
        val morphCharacteristics: Map<String, String> = extractMorph(word)
        return when (getPartOfSpeech(word)) {
            PartOfSpeech.NOUN -> getNounMorphCharacteristics(morphCharacteristics)
            PartOfSpeech.PRONOUN -> getPronounMorphCharacteristics(morphCharacteristics)
            PartOfSpeech.VERB -> getVerbMorphCharacteristics(morphCharacteristics)
            else -> HashMap()
        }
    }

    private fun getNounMorphCharacteristics(morphCharacteristics: Map<String, String>): Map<String, MorphCharacteristic> {
        val resMorphCharacteristics = HashMap<String, MorphCharacteristic>()
        resMorphCharacteristics["Anim"] = getAnimacy(morphCharacteristics["Anim"])
        resMorphCharacteristics["Number"] = getNumber(morphCharacteristics["Number"])
        resMorphCharacteristics["Gender"] = getGender(morphCharacteristics["Gender"])
        return resMorphCharacteristics
    }

    private fun getPronounMorphCharacteristics(morphCharacteristics: Map<String, String>): Map<String, MorphCharacteristic> {
        val resMorphCharacteristics = HashMap<String, MorphCharacteristic>()
        resMorphCharacteristics["Number"] = getNumber(morphCharacteristics["Number"])
        resMorphCharacteristics["Person"] = getPerson(morphCharacteristics["Person"])
        when {
            resMorphCharacteristics["Person"] == (Person.THIRD) ->
                resMorphCharacteristics["Gender"] = getGender(morphCharacteristics["Gender"])
        }
        return resMorphCharacteristics
    }

    private fun getVerbMorphCharacteristics(morphCharacteristics: Map<String, String>): Map<String, MorphCharacteristic> {
        val resMorphCharacteristics = HashMap<String, MorphCharacteristic>()
        resMorphCharacteristics["Tense"] = getTense(morphCharacteristics["Tense"])
        resMorphCharacteristics["Number"] = getNumber(morphCharacteristics["Number"])
        resMorphCharacteristics["Gender"] = getGender(morphCharacteristics["Gender"])
        return resMorphCharacteristics
    }

    private fun getAnimacy(animacy: String?): Animacy {
        return morphConverter.convertAnimacy(animacy ?: "")
    }

    private fun getGender(gender: String?): Gender {
        return morphConverter.convertGender(gender ?: "")
    }

    private fun getNumber(number: String?): Number {
        return morphConverter.convertNumber(number ?: "")
    }

    private fun getPerson(person: String?): Person {
        return morphConverter.convertPerson(person ?: "")
    }

    private fun getTense(tense: String?): Tense {
        return morphConverter.convertTense(tense ?: "")
    }

    private fun extractMorph(word: IndexedWord): Map<String, String> {
        return word.get(CoreAnnotations.CoNLLUFeats::class.java) ?: HashMap()
    }

    private fun extractPartOfSpeech(word: IndexedWord): String {
        return word.get(CoreAnnotations.PartOfSpeechAnnotation::class.java) ?: ""
    }
}