package com.tuneit.jackalope.sentiment.model

abstract class Sentimental(val valence: Double? = null)
