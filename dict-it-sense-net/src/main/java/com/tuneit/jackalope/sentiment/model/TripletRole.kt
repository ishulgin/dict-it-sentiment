package com.tuneit.jackalope.sentiment.model

enum class TripletRole {
    ACTION,
    SUBJECT,
    PRIMARY_OBJECT,
    OTHER_OBJECT,
    X
}