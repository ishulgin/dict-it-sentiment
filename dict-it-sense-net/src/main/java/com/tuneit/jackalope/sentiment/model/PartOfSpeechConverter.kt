package com.tuneit.jackalope.sentiment.model

interface PartOfSpeechConverter {
    fun convert(partOfSpeech: String): PartOfSpeech
}