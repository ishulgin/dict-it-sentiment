package com.tuneit.jackalope.sentiment.model

abstract class CollocationUnit(val word: Word,
                               valence: Double?,
                               val relations: List<CollocationUnit>,
                               val sensesCount: SensesCount) : Sentimental(valence) {
    override fun toString(): String {
        return "$word\nСмыслы(пол./отр./нейтр.): $sensesCount\nВалентность: $valence\nСвязи: $relations"
    }
}

