package com.tuneit.jackalope.sentiment.model

enum class PartOfSpeech {
    /**
     * Существительное
     */
    NOUN,

    /**
     * Глагол
     */
    VERB,

    /**
     * Прилагательное
     */
    ADJECTIVE,

    /**
     * Наречие
     */
    ADVERB,

    /**
     * Сочинительный союз
     */
    CCONJUNCTION,

    /**
     * Подчинительный
     */
    SCONJUNCTION,

    /**
     * Предлог
     */
    PREPOSITION,

    /**
     * Частица
     */
    PARTICLE,

    /**
     * Междометие
     */
    INTERJECTION,

    /**
     * Числительное
     */
    NUMERAL,

    /**
     * Местоимение
     */
    PRONOUN,

    /**
     * Остальное
     */
    X
}
