package com.tuneit.jackalope.sentiment.model

class Adverbial(word: Word,
                valence: Double?,
                relations: List<CollocationUnit>,
                sensesCount: SensesCount) : CollocationUnit(word, valence, relations, sensesCount)
