package com.tuneit.jackalope.sentiment.model

import com.tuneit.jackalope.sentiment.model.morph.*
import com.tuneit.jackalope.sentiment.model.morph.Number

interface MorphConverter {
    fun convertAnimacy (animacy: String): Animacy
    fun convertGender (gender: String): Gender
    fun convertNumber (number: String): Number
    fun convertPerson (person: String): Person
    fun convertTense (tense: String): Tense
}
