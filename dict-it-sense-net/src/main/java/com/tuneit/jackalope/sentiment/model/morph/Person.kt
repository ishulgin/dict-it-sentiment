package com.tuneit.jackalope.sentiment.model.morph

enum class Person : MorphCharacteristic {
    FIRST,
    SECOND,
    THIRD
}