package com.tuneit.jackalope.sentiment.model.morph

enum class Gender : MorphCharacteristic {
    MALE,
    FEMALE,
    NEUTRAL
}