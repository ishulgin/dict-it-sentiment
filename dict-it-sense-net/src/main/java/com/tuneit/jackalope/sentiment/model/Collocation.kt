package com.tuneit.jackalope.sentiment.model

class Collocation(val action: Action?,
                  val subject: Subject?,
                  val `object`: Object?,
                  valence: Double?,
                  val relations: List<CollocationRelation>?) : Sentimental(valence) {
    override fun toString(): String {
        return "Действие: {\n$action\n}\nДействующее лицо: {\n$subject\n}\nОбъект: {\n$`object`\n}\nВалентность: $valence\nСвязи: $relations"
    }
}
