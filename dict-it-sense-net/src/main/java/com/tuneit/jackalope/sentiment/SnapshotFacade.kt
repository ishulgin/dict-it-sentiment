package com.tuneit.jackalope.sentiment

import com.tuneit.jackalope.dict.wiki.engine.core.SenseOptionType
import com.tuneit.jackalope.dict.wiki.engine.core.Tonality
import com.tuneit.jackalope.dict.wiki.engine.utils.WiktionarySnapshot

class SnapshotFacade(private val snapshot: WiktionarySnapshot) {
    /**
     * Находит и возвращает тройку чисел, соответсвующих
     * числу положительных, негативных и нейтральных смыслов,
     * ассоциированных с леммой
     */
    fun getSensesCount(lemma: String): Triple<Int, Int, Int> {
        var positive = 0
        var negative = 0
        var neutral = 0
        snapshot.senses
                .values
                .stream()
                .filter {
                    it.lemma == lemma &&
                            it.getOptionsByType(SenseOptionType.TONALITY).isNotEmpty()
                }
                .forEach {
                    when (it.getOptionsByType(SenseOptionType.TONALITY)[0].option.value() as Tonality) {
                        Tonality.POSITIVE -> positive++
                        Tonality.NEGATIVE -> negative++
                        Tonality.NEUTRAL -> neutral++
                    }
                }
        return Triple(positive, negative, neutral)
    }
}
