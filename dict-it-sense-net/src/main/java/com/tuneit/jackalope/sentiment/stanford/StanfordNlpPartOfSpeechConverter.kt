package com.tuneit.jackalope.sentiment.stanford

import com.tuneit.jackalope.sentiment.model.PartOfSpeech
import com.tuneit.jackalope.sentiment.model.PartOfSpeechConverter

class StanfordNlpPartOfSpeechConverter : PartOfSpeechConverter {
    override fun convert(partOfSpeech: String): PartOfSpeech = when (partOfSpeech) {
        "NOUN", "PROPN" -> PartOfSpeech.NOUN
        "VERB", "AUX" -> PartOfSpeech.VERB
        "ADJ" -> PartOfSpeech.ADJECTIVE
        "ADV" -> PartOfSpeech.ADVERB
        "CCONJ" -> PartOfSpeech.CCONJUNCTION
        "SCONJ" -> PartOfSpeech.SCONJUNCTION
        "ADP" -> PartOfSpeech.PREPOSITION
        "PART" -> PartOfSpeech.PARTICLE
        "INTJ" -> PartOfSpeech.INTERJECTION
        "NUM" -> PartOfSpeech.NUMERAL
        "PRON" -> PartOfSpeech.PRONOUN
        else -> PartOfSpeech.X
    }
}