package com.tuneit.jackalope.sentiment

import edu.stanford.nlp.simple.Sentence

class TextProcessor(private val nlpFacade: NlpLibraryFacade) {
    fun processText(text: String): List<Sentence> {
        val textCollocations = nlpFacade.extractTextCollocations(text)
        TODO("Преобразование List<List<Collocation>> в List<Sentence>")
    }
}