package com.tuneit.jackalope.sentiment.model

class SensesCount (private val positive: Int,
                   private val negative: Int,
                   private val neutral: Int) {
    override fun toString(): String {
        return "$positive/$negative/$neutral"
    }
}