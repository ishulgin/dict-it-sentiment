package com.tuneit.jackalope.sentiment.model

enum class CollocationRelationType {
    COORDINATING,
    SUBORDINATING
}
