package com.tuneit.jackalope.sentiment.model.morph

enum class Tense : MorphCharacteristic {
    PAST,
    PRESENT,
    FUTURE
}
