package com.tuneit.jackalope.sentiment.stanford

import com.tuneit.jackalope.sentiment.NlpLibraryFacade
import com.tuneit.jackalope.sentiment.model.Collocation
import com.tuneit.jackalope.sentiment.model.PartOfSpeech
import com.tuneit.jackalope.sentiment.model.TripletRole
import edu.stanford.nlp.international.russian.process.RussianProcessor
import edu.stanford.nlp.ling.CoreAnnotations
import edu.stanford.nlp.ling.CoreLabel
import edu.stanford.nlp.ling.IndexedWord
import edu.stanford.nlp.pipeline.Annotation
import edu.stanford.nlp.semgraph.SemanticGraph
import edu.stanford.nlp.semgraph.SemanticGraphCoreAnnotations
import edu.stanford.nlp.trees.GrammaticalRelation
import edu.stanford.nlp.util.CoreMap

// TODO valence, relations
class StanfordNlpFacade(private val processor: RussianProcessor, private val wordUtils: WordUtils) : NlpLibraryFacade {

    override fun extractTextCollocations(text: String): List<List<Collocation>> {
        return annotateSentences(text).map { processSentence(it) }
    }

    private fun processSentence(sentence: CoreMap): List<Collocation> {
        return extractSubSentences(sentence).flatMap { extractSentenceCollocations(it) }
    }

    private fun extractSubSentences(sentence: CoreMap): List<CoreMap> {
        val sentenceWords: List<CoreLabel> = sentence.get(CoreAnnotations.TokensAnnotation::class.java)
        val subSentences = ArrayList<CoreMap>()
        var currentSubSentenceWords = ArrayList<String>()
        var previous = sentenceWords[0]
        sentenceWords.forEach {
            when {
                isSubordination(it, previous) -> {
                    currentSubSentenceWords.removeAt(currentSubSentenceWords.size - 1)
                    subSentences.add(annotateSingleSentence(joinWords(currentSubSentenceWords)))
                    currentSubSentenceWords = ArrayList()
                }
                isCoordination(it, previous) -> {
                    previous = it
                    subSentences.add(annotateSingleSentence(joinWords(currentSubSentenceWords)))
                    currentSubSentenceWords = ArrayList()
                }
                else -> {
                    currentSubSentenceWords.add(it.value())
                    previous = it
                }
            }
        }
        subSentences.add(annotateSingleSentence(joinWords(currentSubSentenceWords)))
        return subSentences
    }

    private fun isSubordination(current: CoreLabel, previous: CoreLabel): Boolean {
        return previous.value() in listOf(",") &&
                wordUtils.getPartOfSpeech(IndexedWord(current)) == PartOfSpeech.SCONJUNCTION
    }

    private fun isCoordination(current: CoreLabel, previous: CoreLabel): Boolean {
        return current.value() in listOf(";")
    }

    private fun extractSentenceCollocations(sentence: CoreMap): List<Collocation> {
        val relationsGraph = extractRelations(sentence)

        val roles = processRoles(relationsGraph)

        val actions = getActions(roles)
        val subjects = getSubjects(roles)
        val primaryObjects = getPrimaryObjects(roles)
        val objects = getOtherObjects(roles) + primaryObjects

        val collocationUnitsUtils = CollocationUnitsUtils(wordUtils, relationsGraph)

        val collocations = extractMultipleUnitsCollocations(actions, subjects, primaryObjects, objects, collocationUnitsUtils)
        return when {
            collocations.isNotEmpty() -> collocations
            else -> extractSingleUnitCollocation(actions, subjects, objects, collocationUnitsUtils)
        }
    }

    private fun getActions(roles: Map<TripletRole, List<IndexedWord>>): List<IndexedWord> {
        return roles[TripletRole.ACTION] ?: emptyList()
    }

    private fun getSubjects(roles: Map<TripletRole, List<IndexedWord>>): List<IndexedWord> {
        return roles[TripletRole.SUBJECT] ?: emptyList()
    }

    private fun getPrimaryObjects(roles: Map<TripletRole, List<IndexedWord>>): List<IndexedWord> {
        return roles[TripletRole.PRIMARY_OBJECT] ?: emptyList()
    }

    private fun getOtherObjects(roles: Map<TripletRole, List<IndexedWord>>): List<IndexedWord> {
        return roles[TripletRole.OTHER_OBJECT] ?: emptyList()
    }

    private fun processRoles(graph: SemanticGraph): Map<TripletRole, List<IndexedWord>> {
        return joinMaps(processRoot(graph), joinMaps((processEdgesSources(graph)), processEdgesTargets(graph)))

    }

    private fun verticesFromLeafsToRoot(graph: SemanticGraph): List<Set<IndexedWord>> {
        return graph.leafVertices.map {
            val words = HashSet<IndexedWord>()
            var parent = graph.getParent(it)
            words.add(it)
            while (parent != null) {
                words.add(parent)
                parent = graph.getParent(parent)
            }
            words
        }.toList()
    }

    private fun processRoot(graph: SemanticGraph): Map<TripletRole, List<IndexedWord>> {
        return graph.roots.groupBy {
            when (wordUtils.getPartOfSpeech(it)) {
                in listOf(PartOfSpeech.VERB) -> TripletRole.ACTION
                in listOf(PartOfSpeech.NOUN, PartOfSpeech.PRONOUN) -> TripletRole.SUBJECT
                else -> TripletRole.X
            }
        }
    }

    private fun processEdgesSources(graph: SemanticGraph): Map<TripletRole, List<IndexedWord>> {
        return graph.edgeIterable().groupBy({
            val source = it.source
            val relation = it.relation
            when {
                sourceIsAction(source, relation) -> TripletRole.ACTION
                else -> TripletRole.X
            }
        }, { it.source })
    }

    private fun sourceIsAction(source: IndexedWord, relation: GrammaticalRelation): Boolean {
        return wordUtils.getPartOfSpeech(source) in listOf(PartOfSpeech.VERB) &&
                relation.shortName in listOf("nsubj", "obj", "iobj", "nmod")
    }

    private fun processEdgesTargets(graph: SemanticGraph): Map<TripletRole, List<IndexedWord>> {
        return graph.edgeIterable().groupBy({
            val target = it.target
            val relation = it.relation
            when {
                targetIsAction(target, relation) -> TripletRole.ACTION
                targetIsSubject(target, relation) -> TripletRole.SUBJECT
                targetIsPrimaryObject(target, relation) -> TripletRole.PRIMARY_OBJECT
                targetIsOtherObject(target, relation) -> TripletRole.OTHER_OBJECT
                else -> TripletRole.X
            }
        }, { it.dependent })
    }

    private fun targetIsAction(target: IndexedWord, relation: GrammaticalRelation): Boolean {
        return wordUtils.getPartOfSpeech(target) in listOf(PartOfSpeech.VERB) &&
                relation.shortName in listOf("nmod", "cop")
    }

    private fun targetIsSubject(target: IndexedWord, relation: GrammaticalRelation): Boolean {
        return wordUtils.getPartOfSpeech(target) in listOf(PartOfSpeech.NOUN, PartOfSpeech.PRONOUN) &&
                relation.shortName in listOf("nsubj")
    }

    private fun targetIsPrimaryObject(target: IndexedWord, relation: GrammaticalRelation): Boolean {
        return wordUtils.getPartOfSpeech(target) in listOf(PartOfSpeech.NOUN, PartOfSpeech.PRONOUN) &&
                relation.shortName in listOf("obj", "iobj")
    }

    private fun targetIsOtherObject(target: IndexedWord, relation: GrammaticalRelation): Boolean {
        return wordUtils.getPartOfSpeech(target) in listOf(PartOfSpeech.NOUN, PartOfSpeech.PRONOUN) &&
                relation.shortName in listOf("nmod")
    }

    private fun extractSingleUnitCollocation(actions: List<IndexedWord>,
                                             subjects: List<IndexedWord>,
                                             objects: List<IndexedWord>,
                                             collocationUnitsUtils: CollocationUnitsUtils): List<Collocation> {
        val onlyActionCollocation = actions.map {
            Collocation(collocationUnitsUtils.convertAction(it), null, null, null, null)
        }
        val onlySubjectCollocation = subjects.map {
            Collocation(null, collocationUnitsUtils.convertSubject(it), null, null, null)
        }
        val actionsAndSubjects = onlyActionCollocation + onlySubjectCollocation
        return when {
            actionsAndSubjects.isEmpty() -> objects.map {
                Collocation(null, null, collocationUnitsUtils.convertObject(it), null, null)
            }
            else -> actionsAndSubjects
        }
    }

    private fun extractMultipleUnitsCollocations(actions: List<IndexedWord>,
                                                 subjects: List<IndexedWord>,
                                                 primaryObjects: List<IndexedWord>,
                                                 objects: List<IndexedWord>,
                                                 collocationUnitsUtils: CollocationUnitsUtils): List<Collocation> {
        val graph = collocationUnitsUtils.graph

        val verticesSets = verticesFromLeafsToRoot(graph)

        val mentionedActionsAndSubjects = HashSet<Pair<IndexedWord, IndexedWord>>()
        val mentionedActionsAndObjects = HashSet<Pair<IndexedWord, IndexedWord>>()

        val collocations = ArrayList<Collocation>()

        val actionsAndSubjects = cartesianProduct(actions, subjects)
        val actionsAndObjects = cartesianProduct(actions, objects)

        actionsAndSubjects.forEach({ (action, subject) ->
            val actionAndSubject = Pair(action, subject)
            objects.forEach { `object` ->
                if (verticesSets.any { it.containsAll(listOf(subject, action, `object`)) } &&
                        !primaryObjects.contains(`object`) ||
                        primaryObjects.contains(`object`) &&
                        verticesSets.any { it.containsAll(actionAndSubject.toList()) } &&
                        graph.getParent(`object`) == graph.getParent(subject)) {
                    mentionedActionsAndSubjects.add(actionAndSubject)
                    mentionedActionsAndObjects.add(Pair(action, `object`))
                    val actionRepresentation = collocationUnitsUtils.convertAction(action)
                    val subjectRepresentation = collocationUnitsUtils.convertSubject(subject)
                    val objectRepresentation = collocationUnitsUtils.convertObject(`object`)
                    collocations.add(Collocation(actionRepresentation, subjectRepresentation, objectRepresentation, null, null))
                }
            }
            if (!mentionedActionsAndSubjects.contains(actionAndSubject) &&
                    verticesSets.any { it.containsAll(actionAndSubject.toList()) }) {
                val subjectRepresentation = collocationUnitsUtils.convertSubject(subject)
                val actionRepresentation = collocationUnitsUtils.convertAction(action)
                collocations.add(Collocation(actionRepresentation, subjectRepresentation, null, null, null))
            }
        })

        actionsAndObjects.forEach {
            (action, `object`) ->
            if (verticesSets.any { it.containsAll(setOf(action, `object`)) } &&
                    !mentionedActionsAndObjects.contains(Pair(action, `object`))) {
                val actionRepresentation = collocationUnitsUtils.convertAction(action)
                val objectRepresentation = collocationUnitsUtils.convertObject(`object`)
                collocations.add(Collocation(actionRepresentation, null, objectRepresentation, null, null))
            }
        }

        return collocations
    }

    private fun joinMaps(first: Map<TripletRole, List<IndexedWord>>,
                         second: Map<TripletRole, List<IndexedWord>>): Map<TripletRole, List<IndexedWord>> {
        return (first.keys + second.keys)
                .associateBy({ it }, {
                    ((first[it] ?: emptyList()) + (second[it] ?: emptyList())).distinct()
                })
                .toMap()
    }

    private fun cartesianProduct(first: Collection<IndexedWord>,
                                 second: Collection<IndexedWord>): Collection<Pair<IndexedWord, IndexedWord>> {
        return first.flatMap({ helper -> second.map { Pair(helper, it) } })
    }

    private fun annotateSingleSentence(sentence: String): CoreMap {
        return annotateSentences(sentence)[0]
    }

    private fun annotateSentences(text: String): List<CoreMap> {
        return splitSentences(annotateText(text))
    }

    private fun annotateText(text: String): Annotation {
        return processor.processText(listOf(text))[0]
    }

    private fun splitSentences(text: CoreMap): List<CoreMap> {
        return text.get(CoreAnnotations.SentencesAnnotation::class.java)
    }

    private fun extractRelations(sentence: CoreMap): SemanticGraph {
        return sentence.get(SemanticGraphCoreAnnotations.BasicDependenciesAnnotation::class.java)
    }

    private fun joinWords(words: List<String>): String {
        return words.joinToString(" ")
    }
}
