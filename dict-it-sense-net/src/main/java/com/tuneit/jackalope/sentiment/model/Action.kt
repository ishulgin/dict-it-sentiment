package com.tuneit.jackalope.sentiment.model

class Action(word: Word,
             valence: Double?,
             relations: List<CollocationUnit>,
             sensesCount: SensesCount) : CollocationUnit(word, valence, relations, sensesCount)