package com.tuneit.jackalope.sentiment.model.morph

enum class Number : MorphCharacteristic {
    PLURAL,
    SINGULAR
}