package com.tuneit.jackalope.sentiment.stanford

import com.tuneit.jackalope.sentiment.model.MorphConverter
import com.tuneit.jackalope.sentiment.model.morph.*
import com.tuneit.jackalope.sentiment.model.morph.Number

class StanfordNlpMorphConverter : MorphConverter {
    override fun convertAnimacy(animacy: String): Animacy = when (animacy) {
        "Anim" -> Animacy.ANIM
        else -> Animacy.INANIM
    }

    override fun convertGender(gender: String): Gender = when (gender) {
        "Masc" -> Gender.MALE
        "Fem" -> Gender.FEMALE
        else -> Gender.NEUTRAL
    }

    override fun convertNumber(number: String): Number = when (number) {
        "Plur" -> Number.PLURAL
        else -> Number.SINGULAR
    }

    override fun convertPerson(person: String): Person = when (person) {
        "1" -> Person.FIRST
        "2" -> Person.SECOND
        else -> Person.THIRD
    }

    override fun convertTense(tense: String): Tense = when (tense) {
        "Fut" -> Tense.FUTURE
        "Past" -> Tense.PAST
        else -> Tense.PRESENT
    }
}